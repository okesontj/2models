from apm import *

s = 'http://xps.apmonitor.com'
# s = 'http://localhost'
a = '2_linked_models'

apm(s, a, 'clear all')

apm_load(s, a, '2Models.apm')
csv_load(s, a, 'time.csv')

apm_option(s, a, 'nlc.imode', 6)
apm_option(s, a, 'nlc.nodes', 2)
apm_option(s, a, 'nlc.solver', 3)

apm_info(s, a, 'FV', 'tswitch')
apm_option(s, a, 'tswitch.status', 1)
apm_info(s, a, 'FV', 'tend')
apm_option(s, a, 'tend.status', 1)

apm_info(s, a, 'SV', 'y1')
apm_info(s, a, 'SV', 'x1')
apm_info(s, a, 'SV', 'y2')
apm_info(s, a, 'SV', 'x2')

output = apm(s, a, 'solve')
print output

apm_web(s, a) # Missing x2 and y2 in web viewer

y = apm_sol(s, a) # This also doesn't return the values for x2 and y2

print y['tswitch'][-1]
